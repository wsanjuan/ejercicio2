﻿using System;

namespace Ejercicio2
{

    class Prueba {


        public string Nombre {
            get {return "5";}
        }

    }

    class Motor
    {
        private int numeroCilindros;
        private string tipoCarburador;
        public string TipoCombustible { get; set; }
        
        public int obtenerNumeroCilindros() {
            return numeroCilindros;
        }

        public void asignarNumeroCilindros(int cantidad){
            numeroCilindros = cantidad;
        }

        public string obtenerTipoCarburador(){
            return tipoCarburador;
        }

        public void asignarTipoCarburador(string tipoCarburador){
            this.tipoCarburador = tipoCarburador;
        }

        public void consumirCombustible(ref Tanque tanque, decimal cantidadLitros){
            if ( tanque != null )
            {
                tanque.vaciarTanque(cantidadLitros*(decimal)this.numeroCilindros);
            }
        }
    }

    class Tanque
    {
        private decimal cantidadLitrosCombustible;
        // Si se usa en las funciones como le hago para quitarlo
        const decimal MAXIMO_LITROS = 40;

        public Tanque() {
            cantidadLitrosCombustible = 0;
        }

        public void vaciarTanque(decimal cantidadCombustible){
            if ( cantidadCombustible <= cantidadLitrosCombustible )
            {
                cantidadLitrosCombustible -= cantidadCombustible;
                Console.WriteLine("Total de combustible: " + cantidadLitrosCombustible.ToString());     
                           
            } else {
                cantidadLitrosCombustible = 0;
                Console.WriteLine("Tanque vacio!");
            }            
        }

        public void llenarTanque(decimal cantidadCombustible) {
            if ( cantidadCombustible > 0)
            {
                if ((cantidadCombustible+cantidadLitrosCombustible) <= MAXIMO_LITROS ) {
                    cantidadLitrosCombustible += cantidadCombustible;                
                    Console.WriteLine("Total de combustible : " + cantidadLitrosCombustible.ToString());
                 }else{
                     cantidadCombustible = MAXIMO_LITROS;
                     Console.WriteLine("Tanque lleno!");
                 }            
            }
        }

        public decimal ObtenerLitros() {
            return cantidadLitrosCombustible;
        }
    }

    class Carro
    {
        public string color;
        public string marca;
        public int anyoModelo;
        public int numeroPuertas;
        public decimal precio;
        public Tanque miTanque;
        public Motor miMotor;
        // FALTO litrosiniciales
        public decimal litrosIniciales;

        public Carro() 
        {
            miMotor = new Motor();
            miTanque = new Tanque();
        }

        //public void acelerar()
        public void acelerar()
        {
            Console.WriteLine("Estoy acelerando");
            miMotor.consumirCombustible(ref miTanque, (decimal)0.4);
            Console.WriteLine("Remanente de combustible: " + miTanque.ObtenerLitros().ToString());
        }

        public void frenar()
        {
            Console.WriteLine("Estoy frenando");
        }

        public void obtenerCaracteristicas()
        {
            Console.WriteLine("Caracteristicas del auto");
            Console.WriteLine("Color:" + color);
            Console.WriteLine("Marca:" + marca);
            Console.WriteLine("Año del Modelo:" + anyoModelo.ToString());
            Console.WriteLine("Número de Puertas:" + numeroPuertas.ToString());
            Console.WriteLine("Precio:" + precio.ToString());
            Console.WriteLine("Tipo de Carburador:" + miMotor.obtenerTipoCarburador());
            Console.WriteLine("Número de Cilindros:" + miMotor.obtenerNumeroCilindros().ToString());
            Console.WriteLine("Tipo de Combustible:" + miMotor.TipoCombustible);            
        }
    }

    class Program
    {
        //static void Main(string[] args)
        public static void Main(string[] args)
        {
            Carro miCarro = new Carro();

            miCarro.color = "Rojo";
            miCarro.marca = "Chebrolet";
            miCarro.numeroPuertas = 5;
            miCarro.anyoModelo = 2013;
            miCarro.precio = 150000;
            //miCarro.miMotor.asignarNumeroCilindros(4);
            miCarro.miMotor.asignarTipoCarburador("Inyeccion");
            miCarro.miMotor.TipoCombustible = "Gasolina";
            miCarro.obtenerCaracteristicas();
            miCarro.miTanque.llenarTanque((decimal)40.0);
            miCarro.acelerar();
            miCarro.frenar();
            miCarro.acelerar();
            miCarro.frenar();
            miCarro.acelerar();
            miCarro.frenar();
            miCarro.acelerar();
            miCarro.frenar();
            miCarro.acelerar();
            miCarro.frenar();
            miCarro.acelerar();
        }
    }
}
